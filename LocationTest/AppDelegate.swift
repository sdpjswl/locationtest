//
//  AppDelegate.swift
//  LocationTest
//
//  Created by sudeep on 17/09/16.
//  Copyright © 2016 sudeep. All rights reserved.
//

import UIKit
import CoreLocation
import XCGLogger
//import SwiftLocation

let kLogURLKey = "log_urls"

let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

let customLogger: XCGLogger = {
  // Setup XCGLogger
  let customLogger = XCGLogger.defaultInstance()
  customLogger.xcodeColorsEnabled = true // Or set the XcodeColors environment variable in your scheme to YES
  customLogger.xcodeColors = [
    .Verbose: .lightGrey,
    .Debug: .darkGrey,
    .Info: .darkGreen,
    .Warning: .orange,
    .Error: XCGLogger.XcodeColor(fg: UIColor.redColor(), bg: UIColor.whiteColor()), // Optionally use a UIColor
    .Severe: XCGLogger.XcodeColor(fg: (255, 255, 255), bg: (255, 0, 0)) // Optionally use RGB values directly
  ]
  
  var datName = "\(NSDate().description.stringByReplacingOccurrencesOfString(" ", withString: "_"))"
  datName = "\(datName.stringByReplacingOccurrencesOfString(":", withString: "_"))"
  #if USE_NSLOG // Set via Build Settings, under Other Swift Flags
    customLogger.removeLogDestination(XCGLogger.Constants.baseConsoleLogDestinationIdentifier)
    customLogger.addLogDestination(XCGNSLogDestination(owner: customLogger, identifier: XCGLogger.Constants.nslogDestinationIdentifier))
    customLogger.logAppDetails()
  #else
    let logPath: NSURL = appDelegate.cacheDirectory.URLByAppendingPathComponent("XCGLogger_Default_Log_\(datName).txt")
    customLogger.setup(.Debug, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, writeToFile: logPath)
    
    // save file names to user defaults
    var logURLs = NSUserDefaults.standardUserDefaults().objectForKey(kLogURLKey) as? [String]
    
    if logURLs == nil {
      logURLs = []
    }
    logURLs?.append(logPath.absoluteString)
    NSUserDefaults.standardUserDefaults().setObject(logURLs, forKey: kLogURLKey)
    NSUserDefaults.standardUserDefaults().synchronize()
  #endif
  
  return customLogger
}()

// MARK: - AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate
{
  var window: UIWindow?
  var locationManager: CLLocationManager!
  var currentLocation: CLLocationCoordinate2D?
  
  let cacheDirectory: NSURL = {
    let urls = NSFileManager.defaultManager().URLsForDirectory(.CachesDirectory, inDomains: .UserDomainMask)
    return urls[urls.endIndex - 1]
  }()
  
  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
  {
    customLogger.info("Custom Log - didFinishLaunchingWithOptions \(launchOptions)")
    if launchOptions != nil
    {
      if (launchOptions![UIApplicationLaunchOptionsLocationKey] != nil)
      {
        customLogger.info("Custom Log - App launched with location key")
        if let location = locationManager.location?.coordinate {
          startRegionMonitoring(location)
          customLogger.info("Custom Log - Monitoring region after app launched with location key")
        }
      }
    }
    
    setupLocalNotifications()
    setupLocationManager()
    return true
  }
  
  func setupLocationManager()
  {
    if (locationManager == nil)
    {
      locationManager = CLLocationManager()
      locationManager.delegate = self
      locationManager.pausesLocationUpdatesAutomatically = false
      locationManager.distanceFilter = CLLocationDistance(100.0)
      locationManager.desiredAccuracy = kCLLocationAccuracyBest
      locationManager.requestAlwaysAuthorization()
      
      if #available(iOS 9.0, *) {
        locationManager.allowsBackgroundLocationUpdates = true
      }
    }
  }
  
  func applicationWillTerminate(application: UIApplication)
  {
    if currentLocation == nil
    {
      customLogger.info("Custom Log - applicationWillTerminate: current location nil")
      return
    }
    
    // create a region around current location and monitor enter/exit
    startRegionMonitoring(currentLocation!)
  }
  
  func startRegionMonitoring(location: CLLocationCoordinate2D)
  {
    let region = CLCircularRegion(center: location, radius: 100.0, identifier: "manish")
    region.notifyOnExit = true
    region.notifyOnEntry = true
    locationManager.startMonitoringForRegion(region)
    customLogger.info("Custom Log - started monitoring region \(region.identifier) at \(self.currentLocation)")
  }
  
  // MARK: - Local notifications
  
  func setupLocalNotifications()
  {
    let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
    UIApplication.sharedApplication().registerUserNotificationSettings(settings)
  }
  
  func showLocalNotification(message: String)
  {
    customLogger.info("Custom Log - will show local notification: \(message)")
    let localNotification = UILocalNotification()
    localNotification.alertBody = message;
    localNotification.fireDate = NSDate(timeIntervalSinceNow: 1.0)
    UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    customLogger.info("Custom Log - did show local notification: \(message)")
  }
}

extension AppDelegate
{
  func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
  {
    if status == .AuthorizedAlways {
      locationManager.startUpdatingLocation()
    }
  }
  
  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
  {
    currentLocation = locations.first?.coordinate
    showLocalNotification("didUpdateLocations: \(currentLocation)")
    customLogger.info("Custom Log - didUpdateLocations \(currentLocation)")
  }
  
  func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion)
  {
    showLocalNotification("Entered region: \(region.identifier)")
    customLogger.info("Custom Log - didEnterRegion \(region.identifier)")
  }
  
  func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion)
  {
    showLocalNotification("Exited region: \(region.identifier)")
    customLogger.info("Custom Log - didExitRegion \(region.identifier)")
    
    if let location = manager.location?.coordinate {
      startRegionMonitoring(location)
    }
  }
  
  
}
