//
//  ViewController.swift
//  LocationTest
//
//  Created by sudeep on 17/09/16.
//  Copyright © 2016 sudeep. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
  @IBOutlet var logTextView: UITextView!
  override func viewDidLoad()
  {
    super.viewDidLoad()
    print(self)
    let logURLs = NSUserDefaults.standardUserDefaults().objectForKey(kLogURLKey) as! NSArray
    if logURLs.count <= 1 {
      return
    }
    
    let logURLString = logURLs[logURLs.count - 2] as! String
    let logURL = NSURL(string: logURLString)

    customLogger.info("Custom Log - opening file: \(logURL)")
    
    do {
      let content = try NSString(contentsOfURL: logURL!, encoding: NSUTF8StringEncoding)
      logTextView.text = content as String
    }
    catch let error as NSError {
      customLogger.info("Custom Log - error opening file: \(error.localizedDescription)")
    }
  }
}

